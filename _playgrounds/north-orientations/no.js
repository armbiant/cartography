//==============================================================================
//  General
//==============================================================================

var draw = SVG("world-svg");
var data_origMag = [];
var data_origGrid = [];
var p_orig = [];
var geoNorth = {x: 75.0, y: 15.0};
var magNorth = {x: 57.0, y:25.0};

var grid_data2 = [];
var grid_data3 = [];
var grid_data4 = [];
var grid_data5 = [];
var grid_data6 = [];
var grid_data7 = [];
var grid_data8 = [];
var grid_data9 = [];
var grid_data10 = [];
var grid_data11 = [];

// setup of booleans
var drawingGridCkeckbox = true;
// connection to html
var check_utm = document.getElementById("check-utm");
var no_button_clear = document.getElementById("no-clear");

var polyline_orig = SVG.get('#polyline-orig');
var polyline_origMag = SVG.get('#polyline-origMag');
var polyline_origGrid = SVG.get('#polyline-origGrid');
var points_orig = SVG.get('#points-orig');
var grid = SVG.get('#grid-line');

//==============================================================================
//  Setup
//==============================================================================

draw.node.addEventListener("click", function (e) {addNewPoint(getPointClickedOn(e));}, false);

no_button_clear.addEventListener("click", function () {clearAll();}, false);
check_utm.addEventListener("change", function () {toggleUTM();}, false);


//==============================================================================
//  simplify.js
//==============================================================================

// mourner.github.io/simplify-js

//==============================================================================
//  Functionality
//==============================================================================

// General / Drawing

function getPointClickedOn(e) {
  // cursor point in display coordinates
  var pt_client = draw.node.createSVGPoint();
  pt_client.x = e.clientX;
  pt_client.y = e.clientY;
  // cursor point, translated into svg coordinates
  var pt_svg = pt_client.matrixTransform(draw.node.getScreenCTM().inverse());
  //alert(pt_svg.x);
  return {x:pt_svg.x, y:pt_svg.y};
}

function addNewPoint(pt) {
  //data_orig = [];
    p_orig = [];
    p_orig.push(pt);
	grid.clear();
	
  // geographic north
  data_origGeo = [];
  data_origGeo.push(pt);
  var geoNorthEditX = (geoNorth.x - pt.x);
  var geoNorthEditY = (geoNorth.y - pt.y);
  var scale = 14.0;
  var geoNorthEditNormX =pt.x + geoNorthEditX / Math.sqrt(geoNorthEditX*geoNorthEditX + geoNorthEditY*geoNorthEditY) *scale;
  var geoNorthEditNormY =pt.y + geoNorthEditY / Math.sqrt(geoNorthEditX*geoNorthEditX + geoNorthEditY*geoNorthEditY) *scale;
  var geoNorthEditNorm = {x: geoNorthEditNormX, y: geoNorthEditNormY};
  data_origGeo.push(geoNorthEditNorm);
  
  // magnetic north
  data_origMag = [];
  data_origMag.push(pt);
  var magNorthEditX = (magNorth.x - pt.x);
  var magNorthEditY = (magNorth.y - pt.y);
  var scale = 10.0;
  var magNorthEditNormX =pt.x + magNorthEditX / Math.sqrt(magNorthEditX*magNorthEditX + magNorthEditY*magNorthEditY) *scale;
  var magNorthEditNormY =pt.y + magNorthEditY / Math.sqrt(magNorthEditX*magNorthEditX + magNorthEditY*magNorthEditY) *scale;
  var magNorthEditNorm = {x: magNorthEditNormX, y: magNorthEditNormY};
  data_origMag.push(magNorthEditNorm);
  
  // grid north
  data_origGrid = [];  
  data_origGrid.push(pt);
  gridNorth = {x: pt.x, y:0.0}; 
  var scale = 12.0;
  var gridNorthEditX = gridNorth.x - pt.x;
  var gridNorthEditY = gridNorth.y - pt.y;
  var gridNorthEditNormX = pt.x + gridNorthEditX / Math.sqrt(gridNorthEditX*gridNorthEditX + gridNorthEditY*gridNorthEditY) *scale;
  var gridNorthEditNormY = pt.y + gridNorthEditY / Math.sqrt(gridNorthEditX*gridNorthEditX + gridNorthEditY*gridNorthEditY) *scale;
  var gridNorthEditNorm = {x: gridNorthEditNormX, y: gridNorthEditNormY};
  data_origGrid.push(gridNorthEditNorm);
 
  if (drawingGridCkeckbox) {
		drawUTM(pt);
	}
  
  redrawAll();
}

function drawPoint(pt, parent) {
  parent.circle(1.2).attr({ cx: pt.x, cy: pt.y });
}

function redrawAll() {
  // clear
  polyline_orig.clear();
  polyline_origMag.clear();
  polyline_origGrid.clear();
  points_orig.clear();
  // draw
  polyline_orig.polyline(obj2arr(data_origGeo));
  polyline_origMag.polyline(obj2arr(data_origMag));
  polyline_origGrid.polyline(obj2arr(data_origGrid));
  p_orig.map(function(pt){return drawPoint(pt, points_orig);});
 }

function clearAll() {
  data_origGeo = [];
  data_origGrid = [];
  data_origMag = [];
  p_orig = [];
  redrawAll();
  polyline_orig.clear();
  polyline_origMag.clear();
  polyline_origGrid.clear();
  grid.clear();
}

function toggleUTM() {
	drawingGridCkeckbox = !drawingGridCkeckbox;
}

function drawUTM (pt) {
	grid.clear();
	var xOrigGrid = pt.x;
	var yOrigGrid = pt.y;
	
	grid_data2 = [{x: xOrigGrid - 7.0 , y: yOrigGrid - 5.0}, {x: xOrigGrid + 7.0, y:yOrigGrid - 5.0}];
	grid_data3 = [{x: xOrigGrid - 7.0 , y: yOrigGrid - 2.5}, {x: xOrigGrid + 7.0, y:yOrigGrid - 2.5}];
	grid_data4 = [{x: xOrigGrid - 7.0 , y: yOrigGrid}, {x: xOrigGrid + 7.0, y:yOrigGrid}];
	grid_data5 = [{x: xOrigGrid - 7.0 , y: yOrigGrid + 2.5}, {x: xOrigGrid + 7.0, y:yOrigGrid + 2.5}];
	grid_data6 = [{x: xOrigGrid - 7.0 , y: yOrigGrid + 5.0}, {x: xOrigGrid + 7.0, y:yOrigGrid + 5.0}];
	
	grid_data7 = [{x: xOrigGrid - 5.0 , y: yOrigGrid - 7.0}, {x: xOrigGrid - 5.0, y:yOrigGrid + 7.0}];
	grid_data8 = [{x: xOrigGrid - 2.5 , y: yOrigGrid - 7.0}, {x: xOrigGrid - 2.5, y:yOrigGrid + 7.0}];
	grid_data9 = [{x: xOrigGrid, y: yOrigGrid - 7.0}, {x: xOrigGrid, y:yOrigGrid + 7.0}];
	grid_data10 = [{x: xOrigGrid + 2.5 , y: yOrigGrid - 7.0}, {x: xOrigGrid + 2.5, y:yOrigGrid + 7.0}];
	grid_data11 = [{x: xOrigGrid + 5.0 , y: yOrigGrid - 7.0}, {x: xOrigGrid + 5.0, y:yOrigGrid + 7.0}];
	
	grid.polyline(obj2arr(grid_data2));
	grid.polyline(obj2arr(grid_data3));
	grid.polyline(obj2arr(grid_data4));
	grid.polyline(obj2arr(grid_data5));
	grid.polyline(obj2arr(grid_data6));
	grid.polyline(obj2arr(grid_data7));
	grid.polyline(obj2arr(grid_data8));
	grid.polyline(obj2arr(grid_data9));
	grid.polyline(obj2arr(grid_data10));
	grid.polyline(obj2arr(grid_data11));
} 

//==============================================================================
//  Helper
//==============================================================================

function copy(a) {
  return JSON.parse(JSON.stringify(a));
}

function obj2arr(o) {
  var output = o.map(function (a) {
    return [a.x, a.y];
  });
  return output;
}

function printDataPoints() {
  var output = "[";
  data_orig.forEach(function (p) {
    output += "{x:" + p.x.toFixed() + ",y:" + p.y.toFixed() + "}, ";
  });
  output = output.slice(0, -2);
  output += "]";

  return output;
}
